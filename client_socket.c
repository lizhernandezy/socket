#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
 
int main(int argc, char *argv[])
{
	struct sockaddr_in server;
	char ipag[16];
	int descriptr, numbytes,PORT;
	char bufmensrecib[2048];
	char buff[2048];
	system("clear");
	
	strcpy(ipag,argv[1]);
	PORT = atoi(argv[2]);

	if ((descriptr=socket(AF_INET, SOCK_STREAM, 0))==-1){ //Socket
		printf("\nError en socket()\n");
		exit(-1);
	}
 	
	server.sin_family = AF_INET;  //Parametros para servidor
	server.sin_port = htons(PORT);
	server.sin_addr.s_addr=inet_addr(ipag);
	bzero(&(server.sin_zero),8);
 
	if(connect(descriptr, (struct sockaddr *)&server, //Conexión al server
	sizeof(struct sockaddr))==-1){
	printf("\nError en connect()\n");
	exit(-1);
	}
	printf("\n\n\t>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	printf("\n\tConexion Establecida por IP --> %s, PUERTO --> %d",ipag,PORT);

	if ((numbytes=recv(descriptr,buff,2048,0)) == -1){ //Recibe mensaje
		printf("\nError en recv() \n");
		exit(-1);
	}
	printf("\t\t \n %s \n",buff);

	strcpy(bufmensrecib,"SistemasOperativos/Socket");//Cadena para server
	int longbufmensenv = strlen(bufmensrecib);
	int zebufmensenv = sizeof(bufmensrecib);
	printf("\tMensaje a enviar --> %s \n\tLongitud --> %d \n\tBytes --> %d \n",bufmensrecib,longbufmensenv,zebufmensenv); 
	send(descriptr,bufmensrecib,2048,0);

	recv(descriptr,buff,2048,0);
	printf("\tMensaje de Servidor --> %s \n",buff);
	printf("\tComunicación finalizada");
	printf("\n\t>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");

close(descriptr);
}