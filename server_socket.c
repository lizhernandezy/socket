#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#define longbuf 2048
 
int main(int argc, char **argv)
{

	struct sockaddr_in server;
	struct sockaddr_in client;
	
	int descriptr,fd2,lengt_client,numbytes, PORT;
	char ipag[16];
	char bufmensrecib[longbuf];
	char bufmensrecibd[longbuf];
	char buf_mess_send[longbuf]; 
	strcpy(bufmensrecibd,"B y e");
	strcpy(ipag,argv[1]);
	PORT = atoi(argv[2]);
	printf("\n\t>>>>>>>>>>><<<<<<<<<<<");
	printf("\n\tDATOS DEL SERVIDOR\n\tIP --> %s \n\tPUERTO --> %d \n",ipag,PORT);
	printf("\t>>>>>>>>>>><<<<<<<<<<<");	
 
	server.sin_family= AF_INET;
	server.sin_port = htons(PORT);
	server.sin_addr.s_addr=inet_addr(ipag);
	bzero(&(server.sin_zero),8);
 

	if (( descriptr=socket(AF_INET,SOCK_STREAM,0) )<0)      //Socket
	{
		perror("\n Error de apertura de socket");
		exit(-1);
	}
 

	if(bind(descriptr,(struct sockaddr*)&server, sizeof(struct sockaddr))==-1) //Bind
	{
		printf("\n Error en bind() \n");
		exit(-1);
	}
 
	if(listen(descriptr,5) == -1)  //Listen
	{
		printf("\n Error en listen()\n");
		exit(-1);
	}
    printf("\n\n\n\t>>>>>>>>>>>><<<<<<<<<<<<");
	printf("\n\tServidor Funcionando\n");
	printf("\tEsperando Nueva Conexion\n");
	lengt_client= sizeof(struct sockaddr_in);
	if ((fd2 = accept(descriptr,(struct sockaddr *)&client,&lengt_client))==-1) {
		printf("Error en accept()\n");
		exit(-1);
	}

	printf("\tCliente conectado");
	printf("\n\t>>>>>>>>>>>><<<<<<<<<<<<");
	strcpy(buf_mess_send,"\tConectado con el servidor");
	send(fd2, buf_mess_send, 2048,0);
 	

	
	recv(fd2,bufmensrecib,2048,0);
	printf("\n\n\n\tMensaje del Cliente --> %s \n",bufmensrecib);
	printf("\n\n\t>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<");
	printf("\n\t\tDATOS DE MENSAJE PARA EL CLIENTE\n");
	int leng_buf_mess_send2 = strlen(bufmensrecibd);
	int size = sizeof(bufmensrecibd);
	printf("\t\tMensaje --> %s \n",bufmensrecibd);
	printf("\t\tTamaño --> %d Peso --> %d bytes\n",leng_buf_mess_send2, size);
	send(fd2,bufmensrecibd,2048,0);
	printf("\n\t>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<");

    printf("\n\n\n\t>>>>>>>>>>>><<<<<<<<<<<<");
    printf("\n\tMensaje enviado\n");
	printf("\tComunicación finalizada");
    printf("\n\t>>>>>>>>>>>><<<<<<<<<<<<\n");

	close(fd2);
	close(descriptr);
return 0;
}